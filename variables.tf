variable "zone" {
  description = "Zone to create resources"
  type        = string
  default     = "ru-central1-a"
}

variable "folder_id" {
  description = "Folder id to create resources"
  type        = string
  default     = "b1g8h5lvth9oahbdgblj"
}

variable "image_id" {
  description = "Instance OS image. Default is Debian 11"
  type        = string
  default     = "fd8s1rt9rlesqptbevhg"
  #default     = "fd8oshj0osht8svg6rfs"
}

variable "service_account_key_file" {
  description = "Path to service account key"
  type = string
  default = "C:\\Terraform\\diplom_keys.json"
}