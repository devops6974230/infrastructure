resource "yandex_compute_instance" "instance_server" {

  allow_stopping_for_update = true
  count                     = var.servers

  name      = "srv-${count.index + 1}"
  hostname  = "srv-${count.index + 1}"
  folder_id = var.folder_id
  zone      = var.zone

  resources {
    cores  = 4
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = var.image_id
      size     = var.disk
    }
  }

  network_interface {
    subnet_id = var.subnet_id
    ipv4      = var.ipv4
    ipv6      = var.ipv6
    nat       = var.nat
  }

  metadata = {
    user-data = "${file(var.user_data)}"
  }

  scheduling_policy {
    preemptible = var.instance_preemptible
  }

}

resource "yandex_compute_instance" "kubernetes_master" {

  allow_stopping_for_update = false
  count                     = var.masters

  name      = "master-node-${count.index + 1}"
  hostname  = "master-node-${count.index + 1}"
  folder_id = var.folder_id
  zone      = var.zone

  resources {
    cores        = var.cores
    memory       = var.memory
  }

  boot_disk {
    initialize_params {
      image_id = var.image_id
      size     = var.disk
      type     = "network-ssd"
    }
  }

  network_interface {
    subnet_id = var.subnet_id
    ipv4      = var.ipv4
    ipv6      = var.ipv6
    nat       = var.nat
  }

  metadata = {
    user-data = "${file(var.user_data)}"
  }

  scheduling_policy {
    preemptible = var.instance_preemptible
  }

}

resource "yandex_compute_instance" "kubernetes_worker" {

  allow_stopping_for_update = true
  count                     = var.workers

  name      = "worker-node-${count.index + 1}"
  hostname  = "worker-node-${count.index + 1}"
  folder_id = var.folder_id
  zone      = var.zone

  resources {
    cores  = 4
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = var.image_id
      size     = var.disk
    }
  }

  network_interface {
    subnet_id = var.subnet_id
    ipv4      = var.ipv4
    ipv6      = var.ipv6
    nat       = var.nat
  }

  metadata = {
    user-data = "${file(var.user_data)}"
  }

  scheduling_policy {
    preemptible = var.instance_preemptible
  }

}

