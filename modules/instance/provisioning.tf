resource "null_resource" "packages" {
  count      = var.servers
  depends_on = [yandex_compute_instance.instance_server]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.instance_server[count.index].network_interface.0.nat_ip_address
  }

  provisioner "remote-exec" {
    inline = [
      "deb http://ppa.launchpad.net/ansible/ansible/ubuntu focal main",
      "sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367",
      "sudo apt update",
      "sudo apt upgrade -y",
      "sudo apt install ansible -y",
      "sudo apt install mc haproxy -y",
      "echo COMPLETED"
    ]
  }
}

resource "null_resource" "copy_ansible_files" {
  count      = var.servers
  depends_on = [yandex_compute_instance.instance_server]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.instance_server[count.index].network_interface.0.nat_ip_address
  }

  provisioner "file" {
    source      = "ansible"
    destination = "/home/${var.ssh_credentials.user}/ansible"
  }

  provisioner "remote-exec" {
    inline = [
      "chown ${var.ssh_credentials.user}:${var.ssh_credentials.user} -R /home/${var.ssh_credentials.user}/ansible"
    ]
  }

}


resource "null_resource" "set_ssh_key" {
  count      = var.servers
  depends_on = [yandex_compute_instance.instance_server]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.instance_server[count.index].network_interface.0.nat_ip_address
  }

  provisioner "file" {
    source      = var.id_rsa
    destination = "/home/${var.ssh_credentials.user}/.ssh/id_rsa"
  }

  provisioner "remote-exec" {
    inline = [
      "chown ${var.ssh_credentials.user}:${var.ssh_credentials.user} ~/.ssh/id_rsa && chmod 600 ~/.ssh/id_rsa"
    ]
  }

}
