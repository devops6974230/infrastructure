output "server_ip" {
  value = yandex_compute_instance.instance_server[*].network_interface.0.nat_ip_address
}

output "server_hostname" {
  value = yandex_compute_instance.instance_server[*].hostname
}

output "master_ip" {
  value = yandex_compute_instance.kubernetes_master[*].network_interface.0.nat_ip_address
}

output "master_hostname" {
  value = yandex_compute_instance.kubernetes_master[*].hostname
}

output "worker_ip" {
  value = yandex_compute_instance.kubernetes_worker[*].network_interface.0.nat_ip_address
}

output "worker_hostname" {
  value = yandex_compute_instance.kubernetes_worker[*].hostname
}