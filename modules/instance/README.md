## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_yandex"></a> [yandex](#requirement\_yandex) | ~> 0.85 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_null"></a> [null](#provider\_null) | n/a |
| <a name="provider_yandex"></a> [yandex](#provider\_yandex) | ~> 0.85 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [null_resource.copy_ansible_files](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.packages](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.set_ssh_key](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [yandex_compute_instance.instance_server](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/compute_instance) | resource |
| [yandex_compute_instance.kubernetes_master](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/compute_instance) | resource |
| [yandex_compute_instance.kubernetes_worker](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/compute_instance) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cores"></a> [cores](#input\_cores) | Number of instance cores | `number` | `2` | no |
| <a name="input_disk"></a> [disk](#input\_disk) | Instance boot HDD size in GB | `number` | `15` | no |
| <a name="input_folder_id"></a> [folder\_id](#input\_folder\_id) | Folder id to create resources | `string` | n/a | yes |
| <a name="input_id_rsa"></a> [id\_rsa](#input\_id\_rsa) | Path to private user key. Use in provosioning | `string` | `"~\\.ssh\\id_rsa"` | no |
| <a name="input_image_id"></a> [image\_id](#input\_image\_id) | OS family to get id image | `string` | n/a | yes |
| <a name="input_instance_preemptible"></a> [instance\_preemptible](#input\_instance\_preemptible) | Make instance preemptible | `bool` | `false` | no |
| <a name="input_ipv4"></a> [ipv4](#input\_ipv4) | Use ipv4 | `bool` | `true` | no |
| <a name="input_ipv6"></a> [ipv6](#input\_ipv6) | Use ipv6 | `bool` | `false` | no |
| <a name="input_masters"></a> [masters](#input\_masters) | Count of kubernetes master nodes | `number` | `1` | no |
| <a name="input_memory"></a> [memory](#input\_memory) | Instance RAM size in GB | `number` | `2` | no |
| <a name="input_nat"></a> [nat](#input\_nat) | Use nat | `bool` | `false` | no |
| <a name="input_servers"></a> [servers](#input\_servers) | Count of server | `number` | `1` | no |
| <a name="input_ssh_credentials"></a> [ssh\_credentials](#input\_ssh\_credentials) | Credentials for connect to instances | <pre>object({<br>    user        = string<br>    private_key = string<br>    pub_key     = string<br>  })</pre> | <pre>{<br>  "private_key": "~/.ssh/id_rsa",<br>  "pub_key": "~/.ssh/id_rsa.pub",<br>  "user": "titov"<br>}</pre> | no |
| <a name="input_subnet_id"></a> [subnet\_id](#input\_subnet\_id) | ID subnet to plug instance | `string` | n/a | yes |
| <a name="input_user_data"></a> [user\_data](#input\_user\_data) | Path to YAML file with user data | `string` | `"./meta.yml"` | no |
| <a name="input_workers"></a> [workers](#input\_workers) | Count of workers nodes | `number` | `1` | no |
| <a name="input_zone"></a> [zone](#input\_zone) | Zone to create resources | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_master_hostname"></a> [master\_hostname](#output\_master\_hostname) | n/a |
| <a name="output_master_ip"></a> [master\_ip](#output\_master\_ip) | n/a |
| <a name="output_server_hostname"></a> [server\_hostname](#output\_server\_hostname) | n/a |
| <a name="output_server_ip"></a> [server\_ip](#output\_server\_ip) | n/a |
| <a name="output_worker_hostname"></a> [worker\_hostname](#output\_worker\_hostname) | n/a |
| <a name="output_worker_ip"></a> [worker\_ip](#output\_worker\_ip) | n/a |
