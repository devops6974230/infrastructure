# Подготовка инфраструктуры

Подготовленная конфигурация по умолчанию создает сетевую инфраструктуру
и 3 инстанса srv, kubernetes-master-node и kubernetes-worker-node

При создании инстансов генерируется файл inventory.ini

После создания инстансов на сервер srv устанавливается и копируется
локальный каталог ansible с inventory.ini, файлом конфигурации и
плейбуками. Для подключения ansible без указания пароля на сервер srv
копируется приватный ключ, публичные ключи тиражируются на все серверы в authorized_keys

## Развертывание инфраструктуры в Яндекс облаке

1. Создайте облако и каталог
1. Склонируйте репозиторий
1. В variables.tf укажите ид каталога облака (folder_id)
1. Создайте публичный и приватный ключ для своего аккаунта в своем облаке и скачайте файл с ключами в формате json
1. Пропишите путь до файла с ключами (key_path) в переменной service_account_key_file (variables.tf)
1. Выполните команды ```terraform init```, ```terraform plan``` и ```terraform apply```
1. После завершения получите ip сервера управления (srv)
1. Зайдите по ssh на сервер управления


## Развертываение необходимого ПО

Ansible уже установлен на сервер чере terraform (modules/instance/provisioning.tf)
Все файлы ansible скопированы в домашний каталог пользователя ~/ansible

1. Зайдите в каталог ~/ansible
```cd ~/ansible```
1. Выполните проверку доступности серверов по именам, которые автоматически заполнен в inventory.ini с помощью модуля ping
```ansible all -m ping```
При первом запуске нужно согласиться добавить запись в known_hosts
1. Для устрановки docker на все машины запустите плейбук docker. Данный плейбук установит докер на все машины и добавит текущего пользователя в группу docker
```ansible-playbook ~/ansible/playbooks/docker.yml```
1. Для установеки gitlab-runner на все машины запсустите плейбук runners
```ansible-playbook ~/ansible/playbooks/runners.yml```

## Установка Kubernetes

1. При подготовке инфраструктуры на сервер srv устанавливается haproxy. Пропишите в кофиг файле haproxy настройки для кластера
```frontend kube-apiserver
        bind *:6443
        mode tcp
        option tcplog
        default_backend kube-apiserver

backend kube-apiserver
        mode tcp
        option tcplog
        server kube-apiserver-1 master-node-1:6443
```
2. Перезапустите haproxy
3. Запустите плейбук установки кубернетес компонентов
```ansible-playbook ~/ansible/playbooks/kubernetes.yml```
4. Вручную выполнить команды на нодах  
```cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF

sudo modprobe overlay

sudo modprobe br_netfilter

cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF

sudo sysctl --system
```
5. Инициализируйте кластер
```sudo kubeadm init --pod-network-cidr=10.244.0.0/16 --control-plane-endpoint "srv-1:6443" --upload-certs```
6. После инициализации подключите worker ноду (TOKEN и CA_CERT_HASH смените на корректные значения)
```kubeadm join srv-1:6443 --token TOKEN --discovery-token-ca-cert-hash CA_CERT_HASH```


## Gitlab-runner 

Gitlab-runner  запускается только на сервере srv

1. Запустите докер контейнер gitlab-runner
```docker run -d --name gitlab-runner --restart always     -v /var/run/docker.sock:/var/run/docker.sock     -v gitlab-runner-config:/etc/gitlab-runner     gitlab/gitlab-runner:latest```
1. В проетке gitlab в настроках CI/CD получите токен для подключения gitlab-runner 
1. Выполните команду, указав вместо GITLAB_RUNNER_TOKEN свой токен
```docker exec -ti gitlab-runner gitlab-runner register -n --url https://gitlab.com/ --registration-token GITLAB_RUNNER_TOKEN --executor docker --description "Deployment Runner" --docker-image "docker:stable" --tag-list deployment --docker-privileged```

Настройка инфраструктуры завершена.
